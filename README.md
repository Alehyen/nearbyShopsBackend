# Backend of the nearby shops challenge

I'll be using Django to build a REST API.

I'll be updating this file as soon as I go through the project.

## Dependencies

In order to get the project to work you need to install the dependencies below :

- Django rest framework:

	```pip install djangorestframework```

- Pymongo:

	```pip install pymongo```

- Corsheaders to accept http requests from client apps:
	
	```pip install django-cors-headers```

- In order to be able to fetch Geospatial queries you need to create an index for the location field of the shops MongoDB database:

	```db.shops.createIndex( { location : "2dsphere" } )```

## Run the project

- You should have the MongoBD server already started.

- Download the project then go to the root folder and type:

	```python manage.py runserver```

- If you want to access the API from the LAN:

	```python manage.py runserver 0.0.0.0:8000```

### but make sure first that you ip adress is specified in the ALLOWED_HOSTS = ["your-ip-adress"] variable in the   nearbyShopsBackend/settings.py file.

## API routes

 - There is one route in the API   "/shops?latitude=<value>&longitude=<value>"

 you can get the list of the nearby shops to your location.
 If one of the parameters is not specified you'll get the list of all the shops




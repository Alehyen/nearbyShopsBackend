from django.contrib import admin
from django.urls import path
from nearbyShopsAPI import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('shops/',views.shopsList.as_view())
]

urlpatterns  = format_suffix_patterns(urlpatterns)

from django.apps import AppConfig


class NearbyshopsapiConfig(AppConfig):
    name = 'nearbyShopsAPI'

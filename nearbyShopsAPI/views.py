from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Shops
from .serializers import shopsSerializer
from django.http import HttpResponse
from .databaseHandler import DatabaseHandler



# this class is responsible for handling the clients http request for the nearby shops 
class shopsList(APIView):

# handle GET request for nearby shops. Takes two parameters latitude and longitude.
# If one of the parameters is not specified we return the list of all the shops in the database 
	def get(self,request):
		if request.query_params.get('latitude',None) is not None and request.query_params.get('longitude',None) is not None:
			shops=DatabaseHandler.getShopsNearbyLocation(float(request.query_params.get('latitude',None)),float(request.query_params.get('longitude',None)))
			serializer = shopsSerializer(shops, many=True)
			return Response(serializer.data)
		else:
			shops=DatabaseHandler.getAllShops()
			serializer = shopsSerializer(shops, many=True)
			return Response(serializer.data)

	def post(self,request):
		pass

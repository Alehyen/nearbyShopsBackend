from pymongo import MongoClient


MONGODB_SERVER = "localhost"
MONGODB_PORT = 27017
MONGODB_DB = "shops"
SHOPS_COLLECTION = "shops"
connection = MongoClient(MONGODB_SERVER, MONGODB_PORT) 
db = connection[MONGODB_DB]
shopsCollection = db[SHOPS_COLLECTION]


# this class is responsible for handling the database connection
class DatabaseHandler():

# get the list of all the shops nearby a location (by a 2KM radius) from the database 
	def getShopsNearbyLocation(latitude,longitude):
		query={'location':{'$near':{'$geometry':{'type':'Point','coordinates':[longitude,latitude]},'$maxDistance':2000,'$minDistance':0}}}
		return shopsCollection.find(query)

# get the list of all the shops in the database
	def getAllShops():
		return shopsCollection.find()


from django.db import models



# the shops model is designed to store the mongodb shops collection.
# no need to send the location of the shops since we will be doing all the calculation for the
# nearby shops in the backend.
class Shops(models.Model):
	_id = models.CharField(max_length= 50)
	picture = models.CharField(max_length= 255)
	name = models.CharField(max_length= 50)
	email = models.CharField(max_length= 50)
	city = models.CharField(max_length= 20)
	
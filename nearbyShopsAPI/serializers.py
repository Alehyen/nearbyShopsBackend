from rest_framework import serializers
from .models import Shops

# the shopsSerializer class is responsible for sending the json information of the shops model
class shopsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Shops
		fields = '__all__'